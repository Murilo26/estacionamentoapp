"use strict";
angular.module('app').controller('gestaoController', gestaoController);
gestaoController.$inject = ['gestaoService'];

function gestaoController(vagasOcupadasService) {

    var v = this;

    v.$onInit = function () {
        v.buscarVeiculos();
    }

    v.veiculos = [];

    v.buscarVeiculos = function () {
        gestaoService.buscarVeiculos().then((veiculos) => {
            v.veiculos = veiculos.data;
        }).catch(() => {});
    }

    v.salvar = function (parametro) {
        console.log("entrou");
        console.log(parametro);

        gestaoService.salvar(parametro).then(() =>{
            v.buscarVeiculos();
        }).catch(()=>{});
    }
}