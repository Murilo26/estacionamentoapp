angular.module("app").service("gestaoService", function ($http) {
    var service = {
        'buscarVeiculos': buscarVeiculos,
        'salvar': salvar
    };

    return service;

    function buscarVeiculos() {
        return $http.get('http://localhost:10000/listar/veiculos', {});
    };

    function salvar(parametro) {
        let metodo = '/estacionamento';
        
        return $http.post('http://localhost:10000/' + metodo, parametro);

    }
});