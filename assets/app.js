var app = angular.module('app', [
    'ui.router',
    'oc.lazyLoad',
    'ngStorage',
    'ui.utils.masks',
]);

app.config(function ($stateProvider, $urlRouterProvider) {

    $stateProvider
        .state('login', {
            url: '/login',
            templateUrl: 'template/login/login.html',
            controller: 'loginController',
            controllerAs: 'login',
            resolve: {
                loadMyFiles: ['$ocLazyLoad', function ($ocLazyLoad) {

                    return $ocLazyLoad.load({
                        name: 'itecSuite',
                        files: [
                            'assets/controller/login/loginController.js',
                            'assets/controller/login/loginService.js'

                        ],
                        cache: false
                    });
                }]
            }
        })
        .state('home', {
            url: '/home',
            templateUrl: 'template/home/home.html',
            controller: 'homeController',
            controllerAs: 'home',
            resolve: {
                loadMyFiles: ['$ocLazyLoad', function ($ocLazyLoad) {

                    return $ocLazyLoad.load({
                        name: 'itecSuite',
                        files: [

                            'assets/controller/home/homeController.js'
                        ],
                        cache: false
                    });
                }]
            }
        })
        .state('home.estacionamento', {
            url: '/estacionamento',
            data: {
                pageTitle: 'Cadastrar Estacionamento'
            },
            templateUrl: 'template/estacionamento/estacionamento.html',
            controller: 'estacionamentoController',
            controllerAs: 'estacionamento',
            resolve: {
                loadMyFiles: ['$ocLazyLoad', function ($ocLazyLoad) {

                    return $ocLazyLoad.load({
                        name: 'itecSuite',
                        files: [
                            'assets/controller/estacionamento/estacionamentoService.js',
                            'assets/controller/estacionamento/estacionamentoController.js'
                        ],
                        cache: false
                    });
                }]
            }
        })
        .state('home.dashboard', {
            url: '/dashboard',
            data: {
                pageTitle: 'Dashboard'
            },

            templateUrl: 'template/dashboard/dashboard.html',
            controller: 'dashboardController',
            controllerAs: 'dashboard',
            resolve: {
                loadMyFiles: ['$ocLazyLoad', function ($ocLazyLoad) {

                    return $ocLazyLoad.load({
                        name: 'itecSuite',
                        files: [
                            'assets/controller/estacionamento/estacionamentoService.js',
                            'assets/controller/dashboard/dashboardController.js'
                        ],
                        cache: false
                    });
                }]
            }
        })
        .state('home.veiculo', {
            url: '/veiculo',
            data: {
                pageTitle: 'Cadastrar veiculo'
            },
            templateUrl: 'template/veiculo/veiculo.html',
            controller: 'veiculoController',
            controllerAs: 'veiculo',
            resolve: {
                loadMyFiles: ['$ocLazyLoad', function ($ocLazyLoad) {

                    return $ocLazyLoad.load({
                        name: 'itecSuite',
                        files: [
                            'assets/controller/veiculo/veiculoService.js',
                            'assets/controller/veiculo/veiculoController.js'
                        ],
                        cache: false
                    });
                }]
            }
        })
        .state('home.gestao', {
            url: '/gestao',
            data: {
                pageTitle: 'Cadastrar'
            },
            templateUrl: 'template/gestao/gestao.html',
            controller: 'gestaoController',
            controllerAs: 'gestao',
            resolve: {
                loadMyFiles: ['$ocLazyLoad', function ($ocLazyLoad) {

                    return $ocLazyLoad.load({
                        name: 'itecSuite',
                        files: [
                            'assets/controller/gestao/gestaoService.js',
                            'assets/controller/gestao/gestaoController.js'
                        ],
                        cache: false
                    });
                }]
            }
        })
});