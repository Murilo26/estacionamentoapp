package io.sgs.gestacionamento;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import springfox.documentation.swagger2.annotations.EnableSwagger2;

/**
 * @author Murilo Rosa
 *
 */

@SpringBootApplication
@EnableSwagger2
public class Estacionamento {

	public static void main(String[] args) {
		SpringApplication.run(Estacionamento.class, args);
	}

}

