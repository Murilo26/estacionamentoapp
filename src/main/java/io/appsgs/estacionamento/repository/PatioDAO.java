package io.sgs.gestacionamento.repository;

import io.sgs.gestacionamento.model.Patio;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

/**
 * @author Murilo Rosa
 *
 */
public interface PatioDAO extends CrudRepository<Patio, Long> {
   
   @Query(value = "select sum(qtdVaga) from Patio")
   public int findByTotalVagas();
   
   @Query(value = "select count(*) as vagasOcupadas from Estacionamento where saida is null")
   public int findByTotalVagasOcupadas();

}
