/**
 * 
 */
package io.sgs.gestacionamento.repository;

import io.sgs.gestacionamento.model.Usuario;

import org.springframework.data.repository.CrudRepository;

/**
 * @author Murilo Rosa
 *
 */

public interface UsuarioDAO extends CrudRepository<Usuario, Long> {

   public Usuario findByEmail(String email);

}
